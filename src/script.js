const AUTO_OPEN_DELAY = 5000;
var url = window.location.pathname.split('/');
const imageList = require('./images/*.jpg')
// cleanup array
url = url.filter(function(el) {
  return el;
});

console.log(imageList)

var label = document.getElementById('label');
var btn = document.getElementById('go-telegram-btn');

function createElement(elName, attrs) {
  /* Example:
      var myDiv = createElement("div", {className: "my-div"});
  */

  var newElement = document.createElement(elName);

  for (var k in attrs) {
      newElement[k] = attrs[k];
  }

  return newElement;
}

function setUserCard(opts) {
  const card = createElement('div', {className: 'card'});
  var description = createElement('div', {className: 'description'});
  description.innerHTML = opts.description;

  const img = createElement('img', {className: 'avatar'});
  img.src=imageList[opts.nick]
  if (img.src) {
    card.appendChild(img);
  }

  card.appendChild(description);

  var main = document.querySelector('#main')
  var firstElement = main.querySelector(':first-child');
  main.insertBefore(card, firstElement);
}

function getDefaultLink(url) {
  str = "tg://resolve?domain=" + url[0] + window.location.search.replace("?start=", "&start=");
  url[1] && (str = str + "&post=" + url[1]);
  return str;
}

if (url.length) {
  var domain = url[0];

  switch (url[0]) {
    case 'xevin':
      setUserCard({
        description: 'i\'m Xevin',
        nick: 'xevin'
      });
      str = getDefaultLink(url);
      break;
    case 'gala_bloknot':
      setUserCard({
        description: '',
        nick:'gala_bloknot'
      });
      str = getDefaultLink(url);
      break;
    case 'gala_kr':
      setUserCard({
        description: '',
        nick:'gala_kr'
      });
      str = getDefaultLink(url);
      break;
    case 'store_handmade':
      setUserCard({
        description: 'Изделия ручной работы "Handmade"',
        nick:'store_handmade'
      });
      str = getDefaultLink(url);
      break;
    case 'socks':
      var str = "tg://socks" + window.location.search;
      break;
    case "joinchat":
      str = "tg://join?invite=" + url[1];
      break;
    case "addstickers":
      str = "tg://addstickers?set=" + url[1];
      break;
    case "proxy":
      str = "tg://" + url[0] + window.location.search;
      break;
    default:
      str = getDefaultLink(url);
  }

  // start animation
  btn.classList.add('progressbar');

  // var timerId = setTimeout(function () {
  //   btn.classList.remove('progressbar');
  //   window.location.replace(str);
  // }, AUTO_OPEN_DELAY);

  label.innerHTML = url[0];
  btn.href = str;
  btn.addEventListener('click', function(e) {
    btn.classList.remove('progressbar');
    clearTimeout(timerId);
  });
} else {
  btn.style.display = 'none';
}
